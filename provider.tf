# Bloque de proveedor para Google Cloud Platform
provider "google" {
   credentials = "${file("./creds/serviceaccount.json")}"  # Cargar las credenciales de un archivo JSON
   project     = "demo2-chek" # Reemplazar con el ID del proyecto de Google Cloud
   region      = "var.region"  # Se agrega region a traves del archivo de variables
   #region      = "us-central1"
 }