# Configurar el backend de Terraform en Google Cloud Storage (GCS)
terraform {
    backend "gcs" {
        # Archivo de credenciales de la cuenta de servicio de GCP
         credentials = "./creds/serviceaccount.json"
        # Nombre del bucket de GCS 
         bucket      = "terraform-bucket-argocd-cluster"
    }
}